﻿using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

public class Bot 
{
	
    public static void Main(string[] args) 
    {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) 
        {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;

			new Bot(reader, writer, new Join(botName, botKey));

           // botId botId = new botId("Ethanols", "W1ucMlCKsqSTWA");
           // string trackName = "germany";
          //  int carCount = 3;

          //  new Bot(reader, writer, new JoinRace(botId, trackName, carCount));
		}
	}

	private StreamWriter writer;

    // Guarda os dados no formato string
    private string s_data;

    // Guarda o angulo atual.
    private float f_angle;

    // Indica o valor do throttle
    private float f_throttle = 1.0f;

    // Guarda a distancia atual na peca
    private float f_actualInPieceDistance = 999;

    // Guarda a distancia anterior na peca
    private float f_previousInPieceDistance = 999;

    // Guarda a maior reta da pista
    private float f_biggestStraightLength = 0;

    // Guarda o raio da curva de crash
    private float f_crashPieceRadius;

    // Conta os ticks
    private int i_contador = 0;

    // Off Set da velocidade maxima
    private float f_MaxVelocitOffSet = 0.3f;

    // Indica o numero da peca
    private int i_numberOfPiece = 0;

    // Indica quantas pecas tem a pista
    private int i_numberOfPieces;

    // Indica quantas lanes tem a pista
    private int i_numberOfLanes = 0;

    // Indica qual a lane atual
    private int i_actualLaneIndex;

    // Indica qual a lane anterior
    private int i_previousLaneIndex;

    // Indica quantos switches tem a pista
    private int i_numberOfSwitches = 0;

    // Indica qual a peca atual do carro
    private int i_actualPieceIndex = 999;

    // Indica a peca que comeca a maior reta
    private int i_bestPieceIndexToBoost = 1000;

    // Indica qual a peca anterior
    private int i_previousPieceIndex = 999;

    // Indica a posicao na data do nosso nome do carro...
    private int i_ourCarNameIndex;

    private int i_ticksToBreak;

    private int x;

    private int y;

    private int l;

    private int f;

    private int r;

    private int s;

    private int a;

    // Guarda a velocidade do carro no tick
    private float f_tickVelocity = 0;

    // Guarda a velocidade do carro no tick 01
    private float f_tick01Velocity = 0;

    // Guarda a velocidade do carro no tick 02, funviona bitch
    private float f_tick02Velocity = 0;

    // Guarda a velocidade do carro no tick 03
    private float f_tick03Velocity = 0;

    // Guarda a velocidade de crash
    private float f_crashVelocity;

    // Guarda a constante K
    private float f_constantK;

    // Guarda a massa do carro
    private float f_carWeight;

    // Guarda o comprimento das pecas
    private float[] array_f_pieceLength = new float[1000];

    // Guarda o raio das pecas
    private float[] array_f_pieceRadius = new float[1000];

    // Guarda o angulo da peca
    private float[] array_f_pieceAngle = new float[1000];

    // Guarda se o pedaco pode mudar de lane
    private bool[] array_b_pieceSwitch = new bool[1000];

    // Indica o melhor lado na lane
    private bool[] array_b_bestLaneRightSide = new bool[1000];

    // Indica a velocidade maxima na peca e na lane
    private float[][] vec2_f_MaxVelocity = new float[4][];
    
    // Guarda a distancia do centro de cada lane
    private float[] array_f_laneDistanceFromCenter = new float[4];

    // Indica se jah crashou em uma curva
    private bool b_isCrashed = false;

    // Indica se existe uma peca de pista
    private bool b_isThereAPiece = true;

    // Indica se existe uma lane de pista
    private bool b_isThereALane = true;

    // Indica se estah na melhor lane
    private bool b_isAtBetterLane = false;

    // Indica se ele vai entrar na direita
    private bool b_isGoingRight = false;

    // Indica se ele vai entrar na esquerda
    private bool b_isGoingLeft = false;

    // Indica se o turbo estah disponivel
    private bool b_isTurboAvailable = false;

    // Indica se os testes estao finalizados
    private bool b_isTestsDone = false;

    // Indica que as velocidades maximas foram calculadas
    private bool b_isMaxVelocitysReady = false;

    // indica se teve uma variacao de angulo pela primeira vez
    private bool b_isThereAnAngleCarAtFirstTime = false;

	Bot(StreamReader reader, StreamWriter writer, Join join) 
    {
		this.writer = writer;
		string line;

		send(join);

        // Inicia o vector
        for (int x = 0; x < vec2_f_MaxVelocity.Length; x++)
        {
            vec2_f_MaxVelocity[x] = new float[1000];
        }

		while((line = reader.ReadLine()) != null) 
        {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);

            switch(msg.msgType) 
            {
				case "carPositions":

                    // Pega a data em string
                    s_data = msg.data.ToString();

                    // Procura a data do carro Ethanols
                    i_ourCarNameIndex = s_data.IndexOf("\"Ethanols\"");

                    // Pega apeca atual, a distancia na peca e a lane atual
                    getActualPieceAndDistanceAndLane();

                    // Calcula a velocidade no tick
                    calculateTickVelocity();

                    // Verifica se deve usar o turbo
                    checkTurbo();

                    // Verifica se o contador eh maior que 4
                    if (i_contador > 4)
                    {
                        // Verifica se deve mudar de lane
                        checkSwitch();
                    }

                    // Pega o angulo do carro
                    getCarAngle();

                    // Checa se faz uma curva pela primeira vez
                    checkFirstTimeAngleCar();

                    Console.WriteLine("Velocidade maxima da peca: " + vec2_f_MaxVelocity[i_actualLaneIndex][i_actualPieceIndex]);

                    if (i_contador == 4)
                    {
                        f_throttle = 0.64962f;
                    }

                    if (i_contador == 1)
                    {
                        if (f_tickVelocity == 0)
                        {
                            i_contador = 0;
                        }

                        f_tick01Velocity = f_tickVelocity;

                        Console.WriteLine("Velocidade tick 01: " + f_tick01Velocity);
                    }

                    else if (i_contador == 2)
                    {
                        f_tick02Velocity = f_tickVelocity;

                        Console.WriteLine("Velocidade tick 02: " + f_tick02Velocity);
                    }

                    else if (i_contador == 3)
                    {
                        f_tick03Velocity = f_tickVelocity;

                        Console.WriteLine("Velocidade tick 03: " + f_tick03Velocity);

                        // Calcula valores da fisica
                        getPhisichs();
                    }

                    send(new Throttle(f_throttle));

                    i_contador++;

                     break;

				case "join":

					Console.WriteLine("Joined");
					send(new Ping());

					break;

				case "gameInit":
					Console.WriteLine("Race init");

                    // Pega a data em string.
                    s_data = msg.data.ToString();

                    // Seta que nao estah na melhor lane
                    b_isAtBetterLane = false;

                    // Guarda todas as lanes
                    getLanes();

                    // Guarda todas as pecas da pista
                    getTrack();

                    // Calcula os melhores lados das lanes
                    calculateBestSideSwitches();

                    // Guarda a peca que comeca a maior reta da pista
                    getBestPieceToBoost();

                    send (new Ping());

					break;

				case "gameEnd":

					Console.WriteLine("Race ended");
					send(new Ping());

					break;
				case "gameStart":

					Console.WriteLine("Race starts");
					send(new Ping());

					break;

                case "crash":

                    Console.WriteLine("Crashou");

                    // Pega a data em string
                    s_data = msg.data.ToString();

                    // Verifica se nosso carro que crashou
                    verifyOurCarCrashed();

                    break;

                case "turboAvailable":

                    Console.WriteLine("Turbo Disponivel");

                    // Seta que o turbo estah disponivel
                    b_isTurboAvailable = true;

                    break;


				default:

					send(new Ping());

					break;
			}
		}
	}

    /// <summary>
    /// Verifica qual deve ser o novo throttle
    /// </summary>
    private void verifyThrottle()
    {
        bool isGoingToCalculateMaxVelocity = false; ;
        bool isSwitching = false;

        float nextPieceMaxVelocity;

        // Verifica se a peca atual eh uma reta
        if (array_f_pieceRadius[i_actualPieceIndex] == 0)
        {
            // Verifica se a proxima peca eh reta
            if (array_f_PieceRadius[i_actualPieceIndex+1] == 0)
            {
                f_throttle = 1.0f;
            }

            // Proxima peca eh uma curva
            else
            {
                // Verifica se nao vai mudar de lane nessa peca
                if (!array_b_pieceSwitch[i_actualPieceIndex])
                {
                    // Verifica se nao vai mudar de lane na proxima peca
                    if (!array_b_pieceSwitch[i_actualPieceIndex + 1])
                    {
                        isGoingToCalculateMaxVelocity = true;
                        isSwitching = false;
                    }

                    // Tem switch e verifica se vai mudar
                    else if (b_isGoingRight || b_isGoingLeft)
                    {
                        isGoingToCalculateMaxVelocity = true;
                        // Seta que vai mudar de lane
                        isSwitching = true;
                    }
                }

                // A peca tem switch
                else
                {
                    // Vai mudar de lane
                    if (b_isGoingRight || b_isGoingLeft)
                    {
                        isGoingToCalculateMaxVelocity = true;
                        // Seta que vai mudar de lane
                        isSwitching = true;
                    }

                    // Nao vai mudar de lane
                    else
                    {
                        isGoingToCalculateMaxVelocity = true;
                    }
                }
            }
        }

        // A peca atual eh uma curva
        else
        {
            
        }

        // Verifica se vai pegar a velocidade max da proxima pista
        if (isGoingToCalculateMaxVelocity)
        {
            // Verifica se vai mudar de lane
            if (isSwitching)
            {
                if (b_isGoingRight)
                {
                    nextPieceMaxVelocity = vec2_f_MaxVelocity[i_actualLaneIndex+1][i_actualPieceIndex + 1];
                }

                else if (b_isGoingLeft)
                {
                    nextPieceMaxVelocity = vec2_f_MaxVelocity[i_actualLaneIndex-1][i_actualPieceIndex + 1];
                }
            }

            // Nao vai mudar de lane
            else
            {
                nextPieceMaxVelocity = vec2_f_MaxVelocity[i_actualLaneIndex][i_actualPieceIndex + 1];
            }

            // Verifica se a velocidade maxima da proxima peca eh maior que a velocidade maxima aual
            if (nextPieceMaxVelocity > vec2_f_MaxVelocity[i_actualLaneIndex][i_actualPieceIndex])
            {
                if (vec2_f_MaxVelocity[i_actualLaneIndex][i_actualPieceIndex] - 1.0f > f_tickVelocity)
                {
                    f_throttle = 1.0f;
                }

                else
                {
                    float h = vec2_f_MaxVelocity[i_actualLaneIndex][i_actualPieceIndex] * f_constK;

                    f_throttle = h;
                }
            }

            else
            {
                i_ticksToBreak = Convert.ToInt32(- Math.Log((nextPieceMaxVelocity - (f_throttle/f_constK)) / (f_tickVelocity -(f_throttle/f_constK))) * f_carWeight / f_constK);

                float distanceToSlowDown = (f_carWeight / f_constK) * (f_tickVelocity - (f_throttle/f_constK)) * ((float)Math.Pow(2.72, (-f_constK * i_ticksToBreak / f_carWeight))) + (f_throttle * i_ticksToBreak / f_constK);

                // Verifica se eh uma curva
               // if (array_f_pieceRadius[i_actualPiece] != 0)
               // {
                  //  float pieceLength = (float)Math.Abs(0.0174532925f * (array_f_pieceRadius[i_actualPieceIndex] + array_f_laneDistanceFromCenter[i_actualLaneIndex]) * array_f_pieceAngle[i_previousPieceIndex])
               // }

                if (distanceToSlowDown < array_f_pieceLength[i_actualPieceIndex] - f_actualInPieceDistance)
                {
                    f_throttle = 0.0f;
                }

                else
                {
                    if (vec2_f_MaxVelocity[i_actualLaneIndex][i_actualPieceIndex] - 1.0f > f_tickVelocity)
                    {
                        f_throttle = 1.0f;
                    }

                    else
                    {
                        float h = vec2_f_MaxVelocity[i_actualLaneIndex][i_actualPieceIndex] * f_constK;

                        f_throttle = h;
                    }
                }

            }
        }
        
    }

    /// <summary>
    /// Verifica se nosso carro crashou
    /// </summary>
    private void verifyOurCarCrashed()
    {
        // Procura a data do carro Ethanols
        i_ourCarNameIndex = s_data.IndexOf("\"Ethanols\"");

        // Verifica se nosso carro crashou
        if (i_ourCarNameIndex != -1 && !b_isCrashed)
        {
            // Seta que jah crashou em uma curva
            b_isCrashed = true;

            // Guarda a velocidade do crash
            f_crashVelocity = f_tickVelocity;

            // Guarda o raio de crash
            float pieceRadius;

            // Verifica se o raio eh positivo
            if (array_f_pieceRadius[i_actualPieceIndex] > 0)
            {
                pieceRadius = array_f_pieceRadius[i_actualPieceIndex] - array_f_laneDistanceFromCenter[i_actualLaneIndex];
            }

            // Raio eh negativo
            else
            {
                pieceRadius = array_f_pieceRadius[i_actualPieceIndex] + array_f_laneDistanceFromCenter[i_actualLaneIndex];
            }

            // Guarda o raio da curva de crash
            f_crashPieceRadius = pieceRadius;

           // Console.WriteLine("Velocidade de crash: " + f_crashVelocity);

          //  Console.WriteLine("Raio da curva de crash: " + f_crashPieceRadius);
            
            // Seta o novo throttle
          //  f_throttle = 0.64962f;
        }
    }

    /// <summary>
    /// Pega a fisica da pista
    /// </summary>
    private void getPhisichs()
    {
        // Calculo da Constante de arrasto
        f_constantK = ((f_tick01Velocity - (f_tick02Velocity - f_tick01Velocity)) / (f_tick01Velocity * f_tick01Velocity)) * 1.0f;

        Console.WriteLine("K: " + f_constantK);

        // Percorre todas as pecas
        for (int i = 0; i < i_numberOfPieces; i++)
        {
            // Verifica se eh uma reta
            if (array_f_pieceRadius[i] != 0)
            {
                // Percorre todas as lanes
                for (int p = 0; p < i_numberOfLanes; p++)
                {
                    // Guarda a velocidade maxima
                    vec2_f_MaxVelocity[p][i] = 1.0f / f_constantK;
                }
            }
        }

        // Calcula a massa do carro
        f_carWeight = (float)(1 / (Math.Log((f_tick03Velocity - (1.0f / f_constantK)) / (f_tick02Velocity - (1.0f / f_constantK))) / -f_constantK));

        Console.WriteLine("Massa do carro: " + f_carWeight);
    }

    /// <summary>
    /// Indica a melhor peca para usar o boost
    /// </summary>
    private void getBestPieceToBoost()
    {
        int firstPiece = 0;

        int lastPiece = 0;

        // Indica se percorreu todas as retas
        bool isDone = false;

        // Indica que voltou a peca 0
        bool isRestarted = false;

        // Guarda o valor total de reta
        float straightLength = 0;

        // Enquanto nao percorrer todas as retas
        while (!isDone)
        {
            // Percorre todas as pecas
            for (int i = firstPiece; i < i_numberOfPieces; i++)
            {
                // Verifica se a peca eh reta
                if (array_f_pieceRadius[i] == 0)
                {
                    // Incrementa o tamanho da reta
                    straightLength += array_f_pieceLength[i];
                }

                // A peca nao eh uma reta
                else
                {
                    // Guarda a ultima peca verificada
                    lastPiece = i;

                    // Verifica se eh a ultima peca
                    if (i + 1 == i_numberOfPieces || isRestarted)
                    {
                        // Termina de verificar
                        isDone = true;
                    }

                    // Termina de somar o tamanho dessa reta
                    i = i_numberOfPieces;
                }

                // Verifica se percorreu todas as pecas
                if (i == i_numberOfPieces - 1)
                {
                    // Reinicia as pecas
                    i = 0;

                    // Seta que reiniciou as pecas
                    isRestarted = true;
                }
            }

            // Verifica se a reta atual eh maior do que a registrada
            if (straightLength > f_biggestStraightLength)
            {
                // Registra o comprimento da reta
                f_biggestStraightLength = straightLength;

                // Guarda a peca do comeco da maior reta
                i_bestPieceIndexToBoost = firstPiece;
            }

            // Reinicia os valores
            straightLength = 0;
            
            // Comeca da proxima peca
            firstPiece = lastPiece + 1;
        }

        Console.WriteLine("Maior comprimento de reta: " + f_biggestStraightLength + ", Peca que comeca a maior reta: " + i_bestPieceIndexToBoost);
     }

    /// <summary>
    /// Pega o angulo do carro
    /// </summary>
    private void getCarAngle()
    {
        // Procura o angulo do carro
        x = s_data.Substring(i_ourCarNameIndex).IndexOf("\"angle\"");

        f = s_data.Substring(i_ourCarNameIndex).Substring(x).IndexOf(",");
        
        // Guarda o angulo do carro
        f_angle = float.Parse(s_data.Substring(i_ourCarNameIndex).Substring(x + 9, f - 9));

        Console.WriteLine("Angulo: " + f_angle);
    }

    /// <summary>
    /// Checa se faz uma curva pela primeira vez
    /// </summary>
    private void checkFirstTimeAngleCar()
    {
        // Verifica se o angulo eh diferente de 0 pela primeira vez
        if (f_angle != 0 && !b_isThereAnAngleCarAtFirstTime)
        {
            Console.WriteLine("AQUIIIIIIIIIIII");

            // Guarda o raio
            float pieceRadius;

            // Verifica se o raio eh positivo
            if (array_f_pieceRadius[i_actualPieceIndex] > 0)
            {
                pieceRadius = array_f_pieceRadius[i_actualPieceIndex] - array_f_laneDistanceFromCenter[i_actualLaneIndex];
            }

            // Raio eh negativo
            else
            {
                pieceRadius = array_f_pieceRadius[i_actualPieceIndex] + array_f_laneDistanceFromCenter[i_actualLaneIndex];
            }

            // Calcula velocidade angular
            float B = 57.29577957f * (f_tickVelocity / pieceRadius);

            Console.WriteLine("B: " + B);

            // Calcula maxima aceleracao angular antes de deslizar
            float BSlip = B - Math.Abs(f_angle);

            Console.WriteLine("BSlip: " + BSlip);


            // Calcula velocidade limite para derrapar
            float Vth = 0.0174532925f * BSlip * pieceRadius;

            Console.WriteLine("Vth: " + Vth);

            // Calcula forca centrifuga
            float FSlip = Vth * (Vth / pieceRadius);

            Console.WriteLine("FSlip: " + FSlip);

            // Percorre todas as pecas
            for (int i = 0; i < i_numberOfPieces; i++)
            {
                // Percorre todas as lanes
                for (int lane = 0; l < i_numberOfLanes; l++)
                {
                    // Verifica se o raio eh positivo
                    if (array_f_pieceRadius[i] > 0)
                    {
                        pieceRadius = array_f_pieceRadius[i] - array_f_laneDistanceFromCenter[lane];

                        // Velocidade Maxima na peca e na lane
                        vec2_f_MaxVelocity[lane][i] = (float)(Math.Sqrt(FSlip * (pieceRadius)));
                    }

                    // Raio eh negativo
                    else if (array_f_pieceRadius[i] < 0)
                    {
                        pieceRadius = array_f_pieceRadius[i] + array_f_laneDistanceFromCenter[lane];

                        // Velocidade Maxima na peca e na lane
                        vec2_f_MaxVelocity[lane][i] = (float)(Math.Sqrt(FSlip * (pieceRadius)));
                    }

                    Console.WriteLine("lane: " + lane + ", Peca: " + i + ", Velocidade Maxima: " + vec2_f_MaxVelocity[lane][i]);
                }
            }

            // Jah fez todos os calculos necessarios
            b_isThereAnAngleCarAtFirstTime = true;
        }
    }

    /// <summary>
    /// Calcula a velocidade no tick
    /// </summary>
    private void calculateTickVelocity()
    {
        // Verifica se tem algum valor anterior
        if (i_previousPieceIndex != 999)
        {
            // Verifica se estava na mesma peca no tick anterior
            if (i_previousPieceIndex == i_actualPieceIndex)
            {
                // Guarda a velocidade
                f_tickVelocity = f_actualInPieceDistance - f_previousInPieceDistance;
            }

            // Estava em uma peca anterior
            else if (i_previousPieceIndex + 1 == i_actualPieceIndex)
            {
                // Verifica se eh uma curva a peca anterior
                if (array_f_pieceRadius[i_previousPieceIndex] != 0)
                {
                    Console.WriteLine("Length da peca anterior: " + (Math.Abs(0.0174532925f * (array_f_pieceRadius[i_previousPieceIndex] + array_f_laneDistanceFromCenter[i_actualLaneIndex]) * array_f_pieceAngle[i_previousPieceIndex])) + ", Distancia na pista anterior: " + f_previousInPieceDistance);

                    // Guarda a velocidade
                    f_tickVelocity = f_actualInPieceDistance +
                        (Math.Abs(0.0174532925f * (array_f_pieceRadius[i_previousPieceIndex] + array_f_laneDistanceFromCenter[i_actualLaneIndex]) * array_f_pieceAngle[i_previousPieceIndex]) - f_previousInPieceDistance);
                }

                // A peca anterior eh uma reta
                else
                {
                    Console.WriteLine("Length da peca anterior: " + array_f_pieceLength[i_previousPieceIndex] + ", Distancia na pista anterior: " + f_previousInPieceDistance);

                    // Guarda a velocidade
                    f_tickVelocity = f_actualInPieceDistance + (array_f_pieceLength[i_previousPieceIndex] - f_previousInPieceDistance);
                }
            }

            // Provavelmente impossivel
            else
            {
                Console.WriteLine("Fudeu, sem velocidade");
            }
        }

        Console.WriteLine("Velocidade: " + f_tickVelocity);
    }

    /// <summary>
    /// Pega a peca atual, a distancia na peca e a lane atual
    /// </summary>
    private void getActualPieceAndDistanceAndLane()
    {
        // Procura o piece index atual
        int pieceIndex = s_data.Substring(i_ourCarNameIndex).IndexOf("\"pieceIndex\"");

        // Procura o fim da peca atual
        f = s_data.Substring(i_ourCarNameIndex).Substring(pieceIndex).IndexOf(",");

        // Guarda qual a peca no tick anterior
        i_previousPieceIndex = i_actualPieceIndex;

        // Guarda o valor da peca atual
        i_actualPieceIndex = Convert.ToInt32(s_data.Substring(i_ourCarNameIndex).Substring(pieceIndex + 14, f - 14));

        Console.WriteLine("Numero da peca atual: " + i_actualPieceIndex);
        
        // Procura a distancia na peca atual
        int inPieceDistance = s_data.Substring(i_ourCarNameIndex).IndexOf("\"inPieceDistance\"");

        // Procura o fim da distancia na peca
        f = s_data.Substring(i_ourCarNameIndex).Substring(inPieceDistance).IndexOf(",");

        // Guarda a distancia na peca no tick anterior
        f_previousInPieceDistance = f_actualInPieceDistance;

        // Guarda a distancia na peca atual
        f_actualInPieceDistance = float.Parse(s_data.Substring(i_ourCarNameIndex).Substring(inPieceDistance + 19, f - 19));

        // Procura a distancia na peca atual
        int startLaneIndex = s_data.Substring(i_ourCarNameIndex).IndexOf("\"startLaneIndex\"");

        // Procura o fim do indice da lane inicial
        f = s_data.Substring(i_ourCarNameIndex).Substring(startLaneIndex).IndexOf(",");

        // Guarda a lane anterior
        i_previousLaneIndex = i_actualLaneIndex;

        // Guarda a lane atual
        i_actualLaneIndex = Convert.ToInt32(s_data.Substring(i_ourCarNameIndex).Substring(startLaneIndex + 18, f - 18));

        // Seta que mudou de lane
        if (i_previousLaneIndex != i_actualLaneIndex)
        {
            // Seta que nao estah indo para nenhuma lane diferente
            b_isGoingRigth = false;
            b_isGoingLeft = false;
        }
    }

    /// <summary>
    /// Verifica se deve usar o turbo
    /// </summary>
    private void checkTurbo()
    {
        // Verifica se o turbo estah disponivel
        if (b_isTurboAvailable)
        {
            // Verifica se a proxima peca eh a melhor para usar o turbo
            if (i_actualPieceIndex + 1 == i_bestPieceIndexToBoost)
            {
                // Verifica se jah passou da metade da peca
                if (f_actualInPieceDistance > (Math.Abs(0.0174532925f * array_f_pieceRadius[i_actualPieceIndex] * array_f_pieceAngle[i_actualPieceIndex]) / 1.5f))
                {
                    // Ativa o turbo
                    send(new Turbo("EEETHHAAANNOOOOOOLLLLLLLLSSSSSSSSSSS"));
                }
            }
        }
    }

    /// <summary>
    /// Verifica se tem que virar e para qual lado
    /// </summary>
    private void checkSwitch()
    {
        // Verifica se a proxima peca tem switch
        if (array_b_pieceSwitch[i_actualPieceIndex + 1])
        {
            // Verifica se nao estah na melhor lane
            if (!b_isAtBetterLane)
            {
                // Verifica se o melhor lado da proxima peca eh a direita
                if (array_b_bestLaneRightSide[i_actualPieceIndex + 1])
                {
                    // Muda de lane para a direita
                    send(new SwitchLane("Right"));

                    // Indica que ele vai para a direita
                    b_isGoingRigth = true;
                }

                // O melhor lado da proxima lane eh a esquerda
                else
                {
                    // Muda de lane para a esquerda
                    send(new SwitchLane("Left"));

                    // Indica que ele vai para a esquerda
                    b_isGoingLeft = true;
                }

                // Seta que estah na melhor lane
                b_isAtBetterLane = true;
            }
        }

        // Verifica se a peca atual tem switch
        else if (array_b_pieceSwitch[i_actualPieceIndex])
        {
            // Verifica se o carro estah na melhor lane
            if (b_isAtBetterLane)
            {
                // Seta que o carro nao estah na melhor lane
                b_isAtBetterLane = false;
            }
        }
    }

    /// <summary>
    /// Calcula o melhor lado em cada switch
    /// </summary>
    private void calculateBestSideSwitches()
    {
        int firstSwitch = 1000;

        int secondSwitch = 1000;

        int startSwitch = 0;

        // Indica quantos switches foram verificados
        int verifiedSwitches = 0;

        float leftLength = 0;

        float rightLength = 0;

        // Verifica se tem pelo menos 1 switch
        if (i_numberOfSwitches > 0)
        {
            // Enquanto o numero de percursos verificados for menor que o numero de switches
            while (verifiedSwitches < i_numberOfSwitches)
            {

                // Percorre todas as pecas
                for (int i = startSwitch; i < i_numberOfPieces; i++)
                {
                    // Verifica se a peca tem switch
                    if (array_b_pieceSwitch[i])
                    {
                        // Verifica se o primeiro switch ainda nao estah guardado
                        if (firstSwitch == 1000)
                        {
                            // Guarda o primeiro switch
                            firstSwitch = i;
                        }

                        // O primeiro switch estah guardado
                        else
                        {
                            // Guarda o segundo switch
                            secondSwitch = i;

                            // Para de percorrer as pecas
                            i = i_numberOfPieces;
                        }
                    }

                    // Verifica se percorreu todas as pecas
                    if (i == i_numberOfPieces - 1)
                    {
                        // Reinicia as pecas
                        i = 0;
                    }
                }

                // Percorre as pecas entre os switches
                for (int i = firstSwitch; i < secondSwitch + 1; i++)
                {
                    // Verifica se o angulo da peca eh positivo
                    if (array_f_pieceAngle[i] > 0)
                    {
                        // Verifica se eh uma das pecas dos swtiches
                        if (i == firstSwitch || i == secondSwitch)
                        {
                            // Adiciona ao length para a direita
                            rightLength += (array_f_pieceLength[i] / 2);
                        }

                        else
                        {
                            // Adiciona ao length para a direita
                            rightLength += array_f_pieceLength[i];
                        }
                    }

                    // Verifica se o angulo da peca eh negativo
                    else if (array_f_pieceAngle[i] < 0)
                    {
                        // Verifica se eh uma das pecas dos swtiches
                        if (i == firstSwitch || i == secondSwitch)
                        {
                            // Adiciona ao length para a direita
                            leftLength += (array_f_pieceLength[i] / 2);
                        }

                        else
                        {
                            // Adiciona ao length para a direita
                            leftLength += array_f_pieceLength[i];
                        }
                    }
                }

                // Verifica se o primeiro switch eh maior do que o segundo
                if (firstSwitch >= secondSwitch)
                {
                    // Percorre do primeiro switch a ultima peca
                    for (int i = firstSwitch; i < i_numberOfPieces; i++)
                    {
                        // Seta as melhores lanes
                        setBestLane(rightLength, leftLength, i);
                    }

                    // `Percorre da peca 0 ateh o segundo switch
                    for (int i = 0; i < secondSwitch; i++)
                    {
                        // Seta as melhores lanes
                        setBestLane(rightLength, leftLength, i);
                    }
                }

                // O primeiro eh menor do que o segundo
                else
                {
                     // Percorre as pecas entre os switches
                    for (int i = firstSwitch; i < secondSwitch; i++)
                    {
                        // Seta as melhores lanes
                        setBestLane(rightLength, leftLength, i);
                    }
                }

                // Reinicia os valores
                if (secondSwitch + 1 == i_numberOfPieces)
                {
                    startSwitch = 0;
                }

                else
                {
                    startSwitch = secondSwitch+1;
                }
                firstSwitch = secondSwitch;
                secondSwitch = 1000;
                leftLength = 0;
                rightLength = 0;
                verifiedSwitches++;
            }
        }
    }

    /// <summary>
    /// Seta a melhor lane da peca
    /// </summary>
    private void setBestLane(float rightLength, float leftLength, int i)
    {
        // Verifica se a melhor lane eh a da direita...
        if (rightLength > leftLength)
        {
            // Seta que a melhor lane eh a da direita.
            array_b_bestLaneRightSide[i] = true;
        }

        // Verifica se a melhor lane eh a da esquerda
        else if (rightLength < leftLength)
        {
            // Seta que a melhor lane eh a da esquerda
            array_b_bestLaneRightSide[i] = false;
        }

        // Sao iguais as lanes
        else
        {
            // Seta que a melhor lane eh a da direita
            array_b_bestLaneRightSide[i] = true;
        }
    }

    /// <summary>
    /// Guarda todas as lanes
    /// </summary>
    private void getLanes()
    {
        // Procura o comeco das lanes
        int i = s_data.IndexOf("\"lanes\": [");

        // Enquanto existir uma lane
        while (b_isThereALane)
        {
            // Verifica se eh a primeira lane
            if (i_numberOfLanes == 0)
            {
                // Procura o comeco da lane
                x = s_data.Substring(i).IndexOf("{");

                // Procura o fim da lane
                y = s_data.Substring(i).IndexOf("}");
            }

            // Nao eh a primeira lane
            else
            {
                x++;
                y++;

                // Procura o comeco da lane
                x = s_data.Substring(i).IndexOf("{", x);

                // Procura o fim da lane
                y = s_data.Substring(i).IndexOf("}", y);
            }

            // Procura a distancia do centro da lane
            l = s_data.Substring(i).Substring(x, y - x).IndexOf("\"distanceFromCenter\": ");

            // Verifica se tem uma lane
            if (l != -1)
            {
                // Procura o fim da distancia do centro da lane
                f = s_data.Substring(i).Substring(x, y - x).IndexOf(",");

                // Guarda a distancia do centro da lane
                array_f_laneDistanceFromCenter[i_numberOfLanes] = float.Parse(s_data.Substring(i).Substring(x, y - x).Substring(l + 22, f - l - 22));
            }

            // Nao tem distance from center
            else
            {
                // Seta que nao existem mais pecas
                b_isThereALane = false;

                // Termina o while
                break;
            }

            // Incrementa o numero de lanes
            i_numberOfLanes++;
        }
    }

    /// <summary>
    /// Guarda todas as pecas da pista
    /// </summary>
    private void getTrack()
    {
         // Procura o comeco das pecas
        int i = s_data.IndexOf("\"pieces\": [");
        
        // Enquanto exisitir uma peca
        while (b_isThereAPiece)
        {
            // Verifica se eh a primeira peca
            if (i_numberOfPiece == 0)
            {
                // Procura o comeco da peca
                x = s_data.Substring(i).IndexOf("{");

                // Procura o fim da peca
                y = s_data.Substring(i).IndexOf("}");
            }

            // Nao eh a primeira peca
            else
            {
                x++;
                y++;

                // Procura o comeco da peca
                x = s_data.Substring(i).IndexOf("{", x);

                //Console.WriteLine("Comeco da peca: " + x);

                // Procura o fim da peca
                y = s_data.Substring(i).IndexOf("}", y);

               // Console.WriteLine("Fim da peca: " + y);
            }
            
            // Procura se existe o length na peca
            l = s_data.Substring(i).Substring(x, y - x).IndexOf("\"length\": ");

           // Console.WriteLine("index do length: " + l);

            // Verifica se existe o length na peca e eh uma reta
            if (l != -1)
            {
                // Procura se existe switch na peca
                s = s_data.Substring(i).Substring(x, y - x).IndexOf("\"switch\": ");

                // Verifica se existe o switch na peca
                if (s != -1)
                {
                    // Procura o fim do length
                    f = s_data.Substring(i).Substring(x, y - x).IndexOf(",");

                    // Guarda o length do pedaco da pista
                    array_f_pieceLength[i_numberOfPiece] = float.Parse(s_data.Substring(i).Substring(x, y - x).Substring(l + 10, f - l - 10));

                    // Guarda o raio da peca
                    array_f_pieceRadius[i_numberOfPiece] = 0.0f;

                    // Guarda o angulo da peca
                    array_f_pieceAngle[i_numberOfPiece] = 0.0f;

                    // Guarda se a peca tem switch
                    array_b_pieceSwitch[i_numberOfPiece] = true;

                    // Incrementa um switch na pista
                    i_numberOfSwitches++;
                }

                // Nao tem switch na peca
                else
                {
                    // Procura o fim do length
                    f = s_data.Substring(i).Substring(x, y - x).Substring(l).IndexOf("},");

                    // Guarda o length do pedaco da pista
                    array_f_pieceLength[i_numberOfPiece] = float.Parse(s_data.Substring(i).Substring(x, y - x).Substring(l + 10));

                    // Guarda o raio da peca
                    array_f_pieceRadius[i_numberOfPiece] = 0.0f;

                    // Guarda o angulo da peca
                    array_f_pieceAngle[i_numberOfPiece] = 0.0f;

                    // Guarda se a peca tem switch
                    array_b_pieceSwitch[i_numberOfPiece] = false;
                }
            }

            // Nao eh uma reta
            else
            {
      
                // Procura o raio na peca
                r = s_data.Substring(i).Substring(x, y - x).IndexOf("\"radius\": ");

                // Verifica se eh uma curva
                if (r != -1)
                {
                   // Console.WriteLine("eh uma curva");

                    // Procura o angulo da curva
                    a = s_data.Substring(i).Substring(x, y - x).IndexOf("\"angle\": ");

                    // Procura o fim do raio
                    f = s_data.Substring(i).Substring(x, y - x).IndexOf(",");

                    // Guarda o raio do pedaco da pista
                    array_f_pieceRadius[i_numberOfPiece] = float.Parse(s_data.Substring(i).Substring(x, y - x).Substring(r + 10, f - r - 10));

                    // Procura se existe switch na peca
                    s = s_data.Substring(i).Substring(x, y - x).IndexOf("\"switch\": ");

                    // Verifica se existe o switch na peca
                    if (s != -1)
                    {
                        f++;

                        // Procura o fim do angulo
                        f = s_data.Substring(i).Substring(x, y - x).IndexOf(",", f);

                        // Guarda o angulo da curva
                        array_f_pieceAngle[i_numberOfPiece] = float.Parse(s_data.Substring(i).Substring(x, y - x).Substring(a + 9, f - a - 9));

                        // Guarda o length do pedaco da pista
                        array_f_pieceLength[i_numberOfPiece] = Math.Abs(0.0174532925f * array_f_pieceRadius[i_numberOfPiece] * array_f_pieceAngle[i_numberOfPiece]);

                        // Guarda se a peca tem switch
                        array_b_pieceSwitch[i_numberOfPiece] = true;

                        // Incrementa um switch na pista
                        i_numberOfSwitches++;
                    }

                    // Nao existe switch na peca
                    else
                    {
                       // Console.WriteLine("Comeco da string Angulo: " + a + " Fim da string Angulo: " + y);

                        // Guarda o angulo da curva
                        array_f_pieceAngle[i_numberOfPiece] = float.Parse(s_data.Substring(i).Substring(x, y - x).Substring(a + 9));

                        // Guarda o length do pedaco da pista
                        array_f_pieceLength[i_numberOfPiece] = Math.Abs(0.01745f * array_f_pieceRadius[i_numberOfPiece] * array_f_pieceAngle[i_numberOfPiece]);

                        // Guarda se a peca tem switch
                        array_b_pieceSwitch[i_numberOfPiece] = false;
                    }
                }

                // Nao eh uma curva
                else
                {
                    // Seta que nao existem mais pecas
                    b_isThereAPiece = false;

                    // Guarda o numero de pecas da pista
                    i_numberOfPieces = i_numberOfPiece + 1;

                    // Termina o while
                    break;
                }
            }

           // Console.WriteLine("Comprimento peca " + i_numberOfPiece + ": " + array_f_pieceLength[i_numberOfPiece] + ", Switch: " + array_b_pieceSwitch[i_numberOfPiece] + ", Raio: " + array_f_pieceRadius[i_numberOfPiece] + ", Angle: " + array_f_pieceAngle[i_numberOfPiece]);

            // Incrementa o numero da peca
            i_numberOfPiece++;
        }
    }

	private void send(SendMsg msg) 
    {
		writer.WriteLine(msg.ToJson());
	}
}

class MsgWrapper 
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) 
    {
    	this.msgType = msgType;
    	this.data = data;
    }
}

abstract class SendMsg 
{
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() 
    {
        return this;
    }

    protected abstract string MsgType();
}

//class botId
//{
    //public string name;
   // public string key;

   // public botId(string name, string key)
   // {
   //     this.name = name;
   //     this.key = key;
   // }
//}

//class JoinRace : SendMsg
//{
  //  public botId botId;
  //  public string trackName;
  //  public int carCount;

  //  public JoinRace(botId botId, string trackName, int carCount)
  //  {
    //    this.botId = botId;
    //    this.trackName = trackName;
    //    this.carCount = carCount;
    //}

   // protected override string MsgType()
   // {
     //   return "joinRace";
   // }
//}

class Join: SendMsg 
{
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) 
    {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() 
    { 
		return "join";
	}
}

class Ping: SendMsg 
{
	protected override string MsgType() 
    {
		return "ping";
	}
}

class Throttle: SendMsg 
{
	public double value;

	public Throttle(double value) 
    {
		this.value = value;
	}

	protected override Object MsgData() 
    {
		return this.value;
	}

	protected override string MsgType() 
    {
		return "throttle";
	}
}

class Turbo : SendMsg
{
    public string activated;

    public Turbo(string activated)
    {
        this.activated = activated;
    }

    protected override Object MsgData()
    {
        return this.activated;
    }

    protected override string MsgType()
    {
        return "turbo";
    }
}

class SwitchLane: SendMsg
{
    public string direction;

    public SwitchLane(string direction)
    {
        this.direction = direction;
    }

    protected override Object MsgData()
    {
        return this.direction;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
}